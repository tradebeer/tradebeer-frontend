import { useState } from 'react';
import './App.css';
import LineChart from './component/LineChart';

const ws = new WebSocket("ws://localhost:8888")
const App = () => {
  const [data, setData] = useState([]);
  ws.onopen = () => {
    console.log("Opened Connection")
  }
  ws.onmessage = (event) => {
    setData(JSON.parse(event.data))
  }

  ws.onclose = () => {
    console.log("Connection closed")
  }
  console.log("data: " + data)

  return (
    <div className="App">
      <LineChart/>
    </div>
  );
}

export default App;
