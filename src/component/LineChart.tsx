import Chart from 'react-apexcharts'

const LineChart = () => {

    const state = {
          
            series: [{
                name: "Desktops",
                data: [10, 41, 35, 51, 49, 62, 69, 91, 148]
            }],
            options: {
              chart: {
                zoom: {
                  enabled: false
                }
              },
              dataLabels: {
                enabled: false
              },
              grid: {
                row: {
                  colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                  opacity: 0.5
                },
              },
              xaxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
              }
            },
          
          
          };
    

    return(
        <Chart options={state.options} series={state.series} type="line" height={350} width={450} />
    )
}

export default LineChart